        <div class="dropdown mx-2">
              <button class="btn btn-flat dropdown-toggle" style="box-shadow: none;" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                  <img width="20px;" class="me-2" src="/assets/img/flags/<?=$locale?>.png" alt="<?=$locale?>">
                  <?php echo $locale === 'en' ? t('en') : t('am' );?>
              </button>
              <ul class="dropdown-menu" style="min-width: 6rem;padding: 0.2rem 0;" aria-labelledby="dropdownMenuButton1">
                <li>
                    <a class="dropdown-item" href="/set-locale.php?locale=<?=$locale === 'en' ? 'am' : 'en'?>">
                        <img width="20px;" class="me-2" src="/assets/img/flags/<?php echo $locale === 'en' ? 'am' : 'en';?>.png" alt="<?php echo $locale === 'en' ? 'am' : 'en';?>">
                        <?php echo $locale !== 'en' ? t('en') : t('am' );?>
                    </a>
                </li>
              </ul>
        </div>