<?php
    session_start();
    include __DIR__ . '/translations.php';

    $locale = 'en';
    if ( array_key_exists( 'locale' , $_SESSION ) ) {
        $locale = $_SESSION[ 'locale' ];
    }

    function t ($string) {
        global $translations;
        global $locale;
        if ( array_key_exists( $locale , $translations ) && array_key_exists( $string , $translations[ $locale ] ) ) {
            return $translations[$locale][ $string ];
        } else if( array_key_exists( $locale , $translations ) ) {
            $translations[ 'en' ][ $string ] = $string;
            $translations[ 'am' ][ $string ] = 'undefined';
//            file_put_contents( __DIR__ .'/translations.json',json_encode( $translations, JSON_PRETTY_PRINT));
        }
            return $string;
    }
?>
