<?php include __DIR__ . '/config.php'; ?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="my-3 my-lg-5">
         <div class=" mt-md-5  container  mb-5">
             <div class="row d-flex align-items-center justify-content-center">
                 <div class=" d-flex justify-content-center">
                     <h1 class="text-center text-primary"><?=t('Contact us')?></h1>
                 </div>

                 <h3 class="text-center text-primary"><?=array_key_exists('success' , $_GET ) ? t('Thank you for your message. It has been sent.') : '' ?></h3>

                 <div class="my-4">
                     <img src="/assets/img/contact-bg.jpg" alt="" class="w-100" height="300px" style="object-fit: cover">
                 </div>

             </div>
            <div>
                <h5 class="text-primary mt-4">
                    <img src="/assets/img/email.png" alt="email" width="30px">
                    <?=t('Email address')?>: <a target="_blank" class="" href="mail:hrayrmelkonyan@gmail.com">hrayrmelkonyan@gmail.com</a></h5>
            </div>

             <form action="/mail-handler.php" method="POST" class="my-4">
                 <input type="hidden" value="contact"  name="emailType" />
                 <div class="row">
                     <div class="col-lg-4 form-group my-3">
                         <input autofocus type="text" class="form-control" name="name" minlength="5" required placeholder="<?=t('Your name')?>" />
                     </div>
                     <div class="col-lg-4 form-group my-3">
                         <input type="text" class="form-control" name="subject" minlength="5" required placeholder="<?=t('Subject')?>" />
                     </div>
                     <div class="col-lg-4 form-group my-3">
                         <input type="email" class="form-control" name="email" minlength="5" required placeholder="<?=t('Email')?>" />
                     </div>
                     <div class="col-lg-12 form-group my-3">
                         <textarea class="form-control" name="message" id="message" cols="20" rows="4" required placeholder="<?=t('Message')?>"></textarea>
                     </div>
                     <div class="col-lg-12 form-group d-flex mt-4 justify-content-center">
                         <button class="btn btn-primary btn-block d-flex"><?=t('Submit')?></button>
                     </div>
                 </div>
             </form>


         </div>
      </section>

             <div class="container-fluid">
                 <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d845.4372314975341!2d44.519954402783924!3d40.1912010932828!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1smashtoc%2060!5e0!3m2!1sen!2sus!4v1679991605565!5m2!1sen!2sus" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
             </div>

      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
