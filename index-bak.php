<?php include __DIR__ . '/config.php'; ?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="">
         <div class="container">
            <h1 class="text-center my-4 my-md-5 text-primary">Տանիք ՀԿ - Roads for Արմենիա</h1>
            <p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
            <p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
         </div>
         <div class=" mt-md-5 mt-4 container">
            <div class="loop owl-carousel carousel-main owl-theme">
               <img class="item" src="https://i.ytimg.com/vi/RlAaJ822-aI/maxresdefault.jpg" />
               <img class="item" src="./assets/img/carousel/3.jpg">
               <img class="item" src="https://live.staticflickr.com/65535/51388547468_0687a50557_b.jpg" />
               <img class="item" src="https://live.staticflickr.com/65535/48354289501_a558a8e046_b.jpg" />
               <img class="item" src="https://gdb.rferl.org/8601F252-0645-4694-B3D9-859F7A22D485_w1080_h608_s.jpg" />
            </div>
         </div>
      </section>
      <section class="container mt-md-5 mt-4">
         <div class="row">
            <div class="col-md">
               <h2 class=" text-primary">Ինչ մենք ենք անում?</h2>
               <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
            </div>
            <div class="col-md">
               <h2 class=" text-primary">Who we are?</h2>
               <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
               <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
               <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
            </div>
         </div>
      </section>
      <section class="container mt-md-5 mt-4">
         <div class="row">
            <div class="col">
               <h2 class="text-center text-primary mb-3 mb-md-4">Նկարներ</h2>
               <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
            </div>
         </div>
         <div class=" mt-md-5 mt-4">
            <div class="owl-carousel owl-carousel-bottom owl-theme">
               <div class="item">
                  <img src="./assets/img/carousel/1.jpg" />
               </div>
               <div class="item">
                  <img src="./assets/img/carousel/2.jpg" />
               </div>
               <div class="item">
                  <img src="./assets/img/carousel/3.jpg" />
               </div>
               <div class="item">
                  <img src="./assets/img/carousel/4.jpg" />
               </div>
            </div>
         </div>
      </section>
      <?php include __DIR__ . '/partials/contributors.php'; ?>
      <?php include __DIR__ . '/partials/contact-form.php'; ?>
      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
