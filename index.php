<?php include __DIR__ . '/config.php'; ?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="my-3 my-lg-5">
         <div class=" mt-md-5 mt-4 container">
          <h1 class="text-center text-primary my-2 my-md-5"><?=t('Train trainers for green, digital, soft skills and competences via innovative training methods.')?></h1>

            <div class="loop owl-carousel carousel-main owl-theme">
               <img class="item img-responsive" src="/assets/img/carousel/6.png" />
               <img class="item img-responsive" src="/assets/img/home-slider/3.jpeg" />
               <img class="item img-responsive" src="/assets/img/carousel/7.jpg" />
               <img class="item img-responsive" src="/assets/img/home-slider/1.jpeg" />
               <img class="item img-responsive" src="/assets/img/carousel/8.jpg" />
            </div>

         </div>
      </section>
      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
