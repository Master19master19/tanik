<?php
    function reject () {
        header( 'Location: /' );
        exit;
    }

    function resolve ( $type ) {
        if ( $type === 'contact' ) {
            header( 'Location: /contact.php?success=true' );
            exit;
        }
        header( 'Location: /' );
        exit;
    }

    if ( array_key_exists(  'emailType', $_GET ) ) {
        $data = $_GET;
    } else if ( array_key_exists(  'emailType', $_POST)) {
        $data = $_POST;
    } else {
        return reject();
    }

    $type = $data[ 'emailType' ];

    if( $type === 'subscription' ) {
        return EmailHandler::subscription( $data );
    } else if( $type === 'contact' ) {
        return EmailHandler::contact( $data );
    } else {
        return reject();
    }


    class EmailHandler {
        static function subscription( $data ) {
            self::validateEmailAndReject( $data );
            $email = $data[ 'email' ];
            self::subscribeUserWithEmail( $email );
            return resolve( 'subscription' );
        }

        static function validateEmailAndReject( $data ) {
            if (! array_key_exists( 'email' , $data) ) {
                return reject();
            }
            $email = $data[ 'email' ];
            if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
                return reject();
            }
        }

        static function validateContactFields( $data ) {
            if (! array_key_exists( 'subject' , $data) ) {
                return reject();
            }

            if (! array_key_exists( 'message' , $data) ) {
                return reject();
            }
            if (! array_key_exists( 'name' , $data) ) {
                return reject();
            }

            if ( strlen( $data[ 'name' ] ) < 3 || strlen( $data[ 'message' ] ) < 3 || strlen( $data[ 'subject' ] ) < 3 ) {
                return reject();
            }

        }
        static function contact( $data ) {
            self::validateEmailAndReject( $data );
            self::validateContactFields( $data );
            self::contactWithValidFields( $data );
            return resolve( 'contact' );
        }

        static function contactWithValidFields($data) {
            $fileDir = __DIR__ . '/data/contact.json';
            $contactList = [];
            try {
                $contactListRaw = file_get_contents( $fileDir );
                $contactList = json_decode( $contactListRaw , true );
            } catch(Exception $e) {}
            array_push($contactList , $data );
            file_put_contents( $fileDir , json_encode($contactList,JSON_PRETTY_PRINT ) );
        }

        static function subscribeUserWithEmail( $validEmail ) {
            $fileDir = __DIR__ . '/data/subscriptions.json';
            $subscriptionList = [];
            try {
                $subscriptionListRaw = file_get_contents( $fileDir );
                $subscriptionList = json_decode( $subscriptionListRaw , true );
            } catch(Exception $e) {}
            array_push($subscriptionList , $validEmail );
            file_put_contents( $fileDir , json_encode($subscriptionList,JSON_PRETTY_PRINT ) );
        }
    }