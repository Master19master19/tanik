<?php include __DIR__ . '/config.php'; ?>
<?php
    $newsRaw = file_get_contents(__DIR__ . '/data/news.json');
    $info = json_decode( $newsRaw , true );
    if ( ! array_key_exists( 'id' , $_GET ) ) {
        header( 'Location: /news.php?error=noid' );
        exit;
    }
    $id = $_GET[ 'id' ];
//    $id = 1;
    $news = [];
    foreach ( $info as $row ) {
        if ( $row[ 'id' ] == $id ) {
            $news = $row;
        }
    }

    if ( ! array_key_exists( 'id' , $news ) ) {
        header( 'Location: /news.php?error=notfound' );
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="my-3 my-lg-5">
         <div class=" mt-md-5  container  mb-5">
             <div class="row d-flex align-items-center justify-content-center">
                 <?php if( array_key_exists('title', $news ) ): ?>
                     <div class=" d-flex justify-content-center">
                         <h1 class="text-center text-primary"><?=$news['title']?></h1>
                     </div>
                <?php endif; ?>
                 <div class="my-4">
                     <img src="<?=$news['img']?>" alt="" class="w-100" style="object-fit: contain;">
                 </div>

             </div>
                    <div class="divider-custom my-3"></div>
             <div class="mb-3">
                 <div>
                     <span class="text-muted"><?=$news['date'];?></span>
                 </div>
             </div>
             <?php if ( $news[ 'description' ] ) : ?>
                <div class="row my-3">
                    <pre><?=$news['description'];?></pre>
                </div>
             <?php endif; ?>
         </div>
      </section>


      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
