<?php include __DIR__ . '/config.php'; ?>
<?php
    $newsRaw = file_get_contents(__DIR__ . '/data/news.json');
    $info = json_decode( $newsRaw , true );
?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="my-3 my-lg-5">
         <div class=" mt-md-5  container  mb-5">
             <div class="row d-flex align-items-center justify-content-center">
                 <div class=" d-flex justify-content-center">
                     <h1 class="text-center text-primary"><?=t('News&Events')?></h1>
                 </div>

                 <div class="my-4">
                     <img src="/assets/img/news-bg.jpeg" alt="" class="w-100" height="300px" style="object-fit: cover">
                 </div>

             </div>
                    <div class="divider-custom my-3"></div>
                <div class="row">

                <?php foreach ( $info as $key => $news ) : ?>
                     <div class="text-white my-4 col-md-6 p-0 align-items-stretch d-flex">
                         <div class=" mx-4 bg-primary  d-flex flex-column news-wrapper">
                             <div class="d-flex flex-column p-0">
                                 <div class="">
                                     <img class="w-100" src="<?=$news['img']?>" alt="" />
                                 </div>
                                 <?php if( array_key_exists('title', $news ) ): ?>
                                 <div class=" p-3 my-3">
                                     <h4><?=$news['title']?></h4>
                                 </div>
                                 <?php endif; ?>
                             </div>
                             <div class="pt-2 d-flex flex-row justify-content-between px-3 mb-3 ">
                                 <div class="">
                                     <span style="font-size: 18px"><?=$news['date']?></span>
                                 </div>
                                 <div class="">
                                     <a class="text-reset" style="font-size: 20px" href="/news-single.php?id=<?=$news['id']?>"><?=t('Read more')?></a>
                                 </div>
                             </div>
                         </div>
                     </div>
                 <?php endforeach; ?>
                </div>

         </div>
      </section>


      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
