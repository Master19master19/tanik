
<section class="section-form container mt-md-5 mt-4">
    <h2 class="text-primary mb-3 mb-md-4 text-center">Support us now</h2>
    <form id="donate-form" class="form">
        <div class="form-group">
            <label>Անուն</label>
            <input type="text" name="name" class="form-control" required />
        </div>
        <div class="form-group">
            <label>Հեռախոսահամար</label>
            <input type="text" name="phone" class="form-control" required />
        </div>
        <div class="form-group">
            <label>Amount</label>
            <input type="number" name="amount" class="form-control" required />
        </div>
        <div class="form-group mt-3">
            <button class="btn btn-primary">Donate</button>
        </div>
        <div class="mt-3 payment-logo-wrapper">
            <img class='payment-logo' src="https://www.acba.am/pics/acba_b_logo.png">
            <img class='payment-logo' src="https://cdn.visa.com/v2/assets/images/logos/visa/blue/logo.png" />
            <img class='payment-logo' src="https://www.mastercard.ru/content/dam/public/mastercardcom/ru/ru/homepage/icons/mastercard-logo-52.svg" />
        </div>
    </form>
</section>