<footer class="text-center text-lg-start text-white  bg-primary pb-3 pb-md-5 pt-2">
  <!-- Section: Links  -->
  <section class="">
    <div class="container text-center text-md-start mt-5">
      <!-- Grid row -->
      <div class="row mt-3">
        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <h6 class="text-uppercase fw-bold mb-4">
            <?=t('About')?>
          </h6>
          <p>
            VET4GSEB project plans to carry out training to 78 national VET providers in the partners’ countries who will improve their training practices and deliver updated training, adapted to the new needs of the market for electricians and installers from the RES sector. The expected impact from the training courses after the end of the project duration is 2400 trained installers per year.
          </p>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            <?=t('Quick Links')?>
          </h6>
          <p>
            <a href="/project.php" class="text-reset"><?=t('The project')?></a>
          </p>
          <p>
            <a href="/partners.php" class="text-reset"><?=t('Partners')?></a>
          </p>
          <p>
            <a href="/news.php" class="text-reset"><?=t('News&Events')?></a>
          </p>
          <p>
            <a href="/results.php" class="text-reset"><?=t('Downloads')?></a>
          </p>
          <p>
            <a href="/training.php" class="text-reset"><?=t('Training Platform')?></a>
          </p>
          <p>
            <a href="/contact.php" class="text-reset"><?=t('Contact')?></a>
          </p>
        </div>
        <!-- Grid column -->


        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4 d-sm-none d-none d-md-block d-lg-block">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4"><?=t('Subscribe Newsletter')?></h6>
            <form action="/mail-handler.php" method="POST">
                <input type="hidden" value="subscription" name="emailType" />
                <div>
                    <div class="form-group">
                        <label for="email"><?=t('Email')?></label>
                        <input type="email" class="form-control" required minlength="4" name="email" />
                    </div>
                    <div class="form-check my-2">
                      <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" required>
                      <label class="form-check-label" for="flexCheckChecked">
                        <small>
                            <?=t('By continuing, you accept the privacy policy')?>
                        </small>
                      </label>
                    </div>
                      <button type="submit" class="btn btn-light"><?=t('Subscribe')?></button>
                </div>
            </form>
        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->
        <div class="row">

          <p class="text-center">
              <?=t('long_about')?>
          </p>

            <div class="text-center my-3 mb-4 my-xl-0 mb-xl-0">
                <a target="_blank" href="https://www.facebook.com/profile.php?id=100091163286293" class="me-2"><img width="50px" src="/assets/img/fb.png" alt="fb-logo"></a>
<!--                <a target="_blank" href="https://www.linkedin.com"><img width="32px" src="/assets/img/linkedin.png" alt="linkedin-logo"></a>-->
            </div>
        </div>
    </div>
  </section>
  <!-- Section: Links  -->
<div class="container mt-md-5 ">

    <div class="row d-flex justify-content-around bg-white p-2">

        <div class="col-lg mx-auto  bg-white">
            <img class="w-75 mx-auto" src="/assets/img/eu-new.jpg" alt="eu">
        </div>
        <div class="col-lg mx-auto  bg-white text-dark d-flex align-items-center">

        <p>
           <strong>
               Disclaimer
           </strong> : Funded by the European Union. Views and opinions expressed are however those of the author(s) only and do not necessarily reflect those of the European Union or [name of the granting authority]. Neither the European Union nor the granting authority can be held responsible for them.
        </p>
        </div>

    </div>
</div>
</footer>