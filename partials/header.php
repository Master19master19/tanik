<?php include __DIR__ . '/../components/logo-row.php'; ?>

<header class="">
    <nav class="navbar navbar-expand-sm navbar-dark bg-primary">
        <div class="container">
<!--            <a class="navbar-brand text-white" href="/">-->
<!--                <p class="mb-0">Տանիք ՀԿ</p>-->
<!--                <small class="header-logo-bottom-text">Արմենիա</small>-->
<!--            </a>-->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto mb-2 mb-lg-0 w-100 justify-content-around">
                    <li class="nav-item">
                        <a class="nav-link text-white" href="/project.php"><?=t('The project')?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="/partners.php"><?=t('Partners')?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="/news.php"><?=t('News&Events')?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="/results.php"><?=t('Results')?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="/training.php"><?=t('Training Platform')?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="/contact.php"><?=t('Contact')?></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
