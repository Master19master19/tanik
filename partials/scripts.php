
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(function() {
        new Carousel();
        new AutoScroll();
        new ScrollTo();
    });

    class Carousel {
        constructor() {
            const selector1 =  $('.loop');
            const selector2 =  $('.owl-carousel-bottom');
            if (selector1?.length ) {
                $('.loop').owlCarousel({
                    center: true,
                    autoHeight: true,
                    items: 1,
                    loop: true,
                    margin: 10,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: false,
                    autoplay:true,
                    responsiveClass: true,
                      responsive: {
                        0: {
                          items: 1,
                          nav: true
                        },
                        600: {
                          items: 1,
                          nav: false
                        },
                        1000: {
                          items: 2,
                          nav: true,
                          loop: true,
                          margin: 0
                        }
                      }
                });
            }

            if (selector2?.length ) {
                $('.owl-carousel-bottom').owlCarousel({
                    margin: 10,
                    loop: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: false,
                    autoplay: true,
                    autoHeight: true,
                    autoWidth: true,
                    items: 1,
                })
            }
        }
    }

    class AutoScroll {
        constructor() {
            var div = $('.autoscrolling');
            var scrollerInterval = setInterval(function() {
                var pos = div.scrollTop();
                div.scrollTop(++pos);
            }, 50)

        }
    }

    class ScrollTo {
        constructor() {
            $('a[href*="#"]').click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
            });
        }
    }

</script>