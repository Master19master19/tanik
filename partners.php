<?php include __DIR__ . '/config.php'; ?>
<?php
    $info = [
        'Georgia' => [
            [
                'title' => 'Energy Efficiency Centre Georgia (EECG)',
                'img' => '/assets/img/eecg.png',
            ]
        ],
        'Bulgaria' => [
            [

            'title'=>'Sofia Energy Centre – SEC – Coordinator',
            'description' => '
Sofia Energy Centre (SEC) was established in 1997 as a successor of European Community Energy Centre Sofia (established 1992) and has gained a lot of experience in the implementation of different European energy projects in Bulgaria. During its activity period the company has been involved very successfully in PHARE, THERMIE, SYNERGY, SAVE, ALTENER, FP4, FP5, FP6,FP7, IEE, ERASMUS, INTERREG, HORIZON 2020 projects and actions.
   In order to ensure the best possible response to different project requirements, SEC has established a small permanent co-ordination team of highly qualified experts and involves relevant specialists depending on the projects’ specifics.
Mission:
   The necessity of rational energy use and decrease of environmentally harmful emissions, while at the same time meeting the growing demand for energy, is the basis of Sofia Energy Centre’s activity.
   Energy efficiency at the final consumer (building, industry, etc.), co-generation, wider use of solar, wind and bio energy are amongst our priority activities.
   Sofia Energy Centre follows the policy of the EU for promotion and implementation of new energy technologies for a more efficient and clean energy use at local, regional and national level aiming to reach sustainable development.
',
            'website' => 'sec.bg',
            'email' => 'sec@sec.bg',
                'img' => '/assets/img/seclogo.jpg',

            ],
            [

            'title'=>'European Labour Institute – ELI
',
            'description' => '
    The European Labour Institute was established in June 2002 as an NGO. Its team is composed of architects, civil engineers, HVAC engineers, electrical and of automations ones, ecologists - 15 experts.  They all have strong professional background – both academic and practical. The main activities of the institute are: investigations, know–how and technology transfer, projects’ development and implementation, consultancies and training. in the field of RES and energy efficiency.

    ELI, as a researching and training organisation, has taken part in several EU projects related to design of EE, implementation of RES, investigation of advanced technologies for the building sectors. As a trainer – the team of the Institute has delivered trainings to more than 900 energy professionals: from SMEs and public institutions on “RES and Energy Efficient Buildings” topics as well as to academicians on How to implement the new scientific energy technologies in the industry. Experts of the Institute have participated in a Consortium for elaboration of the European model for vocational training of electrical installers, building specialists and HVAC technicians. Lately – provided vocational training for digital skills and competences of electrical specialist.

    ELI has developed and published the following publications:

          ₋ A step - by-step Guideline for households at national level to make the homes more energy independent and efficient;

          ₋ A Roll-out and exploitation plan for implementing an European model for vocational training of building experts;

          ₋ HowtoGuides for vocational training of building professionals – electrical and HVAC technicians, installers;

          ₋ A Guide: How to build an Eco Green Village with MILD HOMEs;

          ₋ A Guideline for market facilitators of energy efficient services;

          ₋ A Monograph : Energy Efficient planning of residential territories, as a publisher.

    ELI’s is working with the Bulgaria Chamber of Commerce and Industry,  Sofia Municipality, the Bulgarian Academy of science, the Ministry of regional development, the Ministry of energy the Agency for sustainable energy development and with a a lot of EU partners. It is s a member of the: European Urban Knowledge network, Smart Specialization Platform, Union of Concerned Scientists.
',
            'website' => 'eli-energy.com',
            'email' => 'eli.bulgaria@gmail.com',
                'img' => '/assets/img/eli.jpg',
            ],
            [
                    'title' => 'Chamber of Installation Specialists in Bulgaria - CISB',
                'description' => '
    The Chamber of Installation Specialists in Bulgaria (CISB) is a national branch organization of persons and companies, operating in the fields of heating, ventilation, air conditioning, refrigeration, electrical engineering and automation, water supply and sewerage, gasification, energy saving technologies, renewable energy sources and environmental protection.


    CISB is a legal entity and a non-profit organization, independent of state, governmental, trade-union and other authorities and bodies, and it performs activity to private benefit. CISB keeps a Professional Register of installation specialists, which includes Bulgarian and foreign persons and legal entities performing installation activities in the country.


   A Vocational Training Center operates alongside the Chamber of Installation Specialists, under NAVET license 200512291. The VTC organizes different forms of training in the country and abroad to increase the qualification of the specialists from the installation branch in 18 different professions and also develops qualification and re-qualification programs for the specialists in the installation branch, for example: Technician of Energy Systems & Installations, and Installer of Electrical Systems & Installations. Since 2020, 479 trainees have undergone different levels of training at the VTC in CISB. The members of the Chamber vary between 90 and 120.


   Some of the main priorities of the organization are:

To identify and guarantee of transparency of the activity of the persons and companies performing installation activity
To improve the management of the installation activity, increasing the responsibility of the installer to achieve the essential requirements for the buildings
To facilitate the relations of its members both between them and with the state and public bodies and organizations in national and international aspect.
To assist the increasing the professional training of specialists in the installation industry according to their capabilities.
To provide methodological assistance to its members to achieve the requirements set by state, municipal, private and other structures.

    Among the above mentioned activities CISB organizes various forms of training in the country and abroad designed to improve the skills of specialists in the installation industry.
CISB is a member of GCP Europe, KRIB and Bulgarian Chamber of Commerce and Industry. CISB has honed its expertise through developing and managing different RES and EE energy projects in partnership with national and international organizations. The experts of CISB are experienced in applying integrated approaches, analyses and solutions that encourage industry and citizens to develop and implement EE and RES technologies.
',
                'website' => 'nisbg.org',
                'email' => 'office@nisbg.org',
                'img' => '/assets/img/cisb.png'
            ]
        ],
        'Albania' => [
            [
                'title' => 'National Chamber of Crafts Albania (DHKZ)',
                'img' => '/assets/img/dhkz (1).png',
                'description' => '
   The National Chamber of Crafts was established on December 19, 2017. It is a legal entity that is organized and operates according to the law "On Crafts in the Republic of Albania" No. 70/2016 and does not exert profitable activity. Based in Tirana, the National Chamber of Crafts has a geographical spread throughout the territory of Albania.


   The law provides for the full organization of craft activity and the creation of a dual qualification system, a completely "German" model. The National Chamber of Crafts focuses on the handicraft activity in the Republic of Albania, the professional qualification in this field, the organization of the handicraft subjects, and the promotion of the development of handicrafts.


   National Chamber of Crafts values individual work, especially the profession. The number of craftsmen in Albania is very large, therefore their identification and qualification will enable a comprehensive and unified development in the labor market.
',
                'website' => 'dhkz.org.al',
                'email' => 'nfo@dhkz.org.al'
            ],
            [
                    'title' => 'People in Focus (PIF)',
                'img' => '/assets/img/pif.png',
                'description' => '
   People in Focus (PiF) is a nonprofit, non-governmental, non-political and independent organization based in Tirana, Albania. PiF works in the field of human rights and social inclusion, intercultural dialogue, active citizenship, democracy media literacy and youth empowerment. Moreover its engagement follows on sustainable environment and labour market through VET and social entrepreneurship.


   The organisation scope is to support and represent Albanian society to give voice to citizens’ concerns, promote human rights and dialogue. Through the years, People in focus has developed expertise and know – how within regional and European projects empowering young people and citizens tackling community issues according to well-defined methodologies.


   PiF aims to play a particularly powerful role as an enabler and constructive challenger, creating the political and social space for collaborations that are based on the core values of trust, service and the collective good.
',
                'website' =>'peopleinfocus.org',
                'email' => 'peopleinfocusalbania|@gmail.com'
            ]
        ],
        'Turkey' => [
            [
                'title' => 'SUSTAINABLE DEVELOPMENT AND ENVIRONMENT ASSOCIATION (SUDEAS)',
                'img' => '/assets/img/sudeas.png',
                'description' => '
   This activity certificate has been prepared to be given to ECAS (European Union Project System) upon the request of the association.


   Sustainable Development and Environment Association is a non-governmental organization established in 2008. Its main objective is to ensure the continuation of the development and industrialization processes of societies and countries and to leave a livable world as a legacy for future generations without damaging the environment. In this context the targets of association are determined shortly;


Createing awareness in society
Drawin attention to global climate change
Expanding sustainable development tools based on environmental protection
Developing sustainable technologies such as low carbon and zero emissions and contributing to its spread
For this purpose, a visionary mission was developed with all our members.


Our vision:


To be at the forefront of sustainable development and environmental issues, both at national and international level, through education, entrepreneurship, innovation and technology transfer.


Our mission:


To develop social awareness about the environment together with the elements of sustainable development, to provide effective services with a holistic approach that covers all stakeholders of the society (public, industry and society).


SUEDAS has realized several projects in national and international as well, some projects are listed and detailed below:

EEMS4SME Project (Erasmus), focused on creating e-learning modules for the technical people to learn and implement the energy management principles on the SME’s. E-learning modules were created for the people to reach the information whenever they are available without considering the place or the time. Certification process has also added to qualify specialist for energy efficiency market.
Ecology Literacy project aims to raise the awareness of the individuals to look critically at natural phenomena and suggest solutions for more sustainable world.
VET4GSEB Project (Erasmus), the aim of the project is to create and develop a network of VET providers on PV systems in order to improve their knowledge and competence for the green transition and the digital transformation in the electrical energy systems.',
                'website' => 'sudeas.org',
                'email' =>'mzsogut@gmail.com',
            ]
        ],
        'Ukraine' => [
            [
                'title' => 'National University of Life and Environmental Sciences of Ukraine (NUBIP)',
                'img' => '/assets/img/nubip.png',
                'description' => '
   According to the status of higher education institutions, the National University of Life and Environmental Sciences of Ukraine has IV accreditation level, and is a research institution, which conducts educational; scientific and research; scientific and innovative; educational and industrial; and information and consulting activities aimed at studying the contemporary science issues about life and environment; at usage, reproduction and balanced development of terrestrial and aquatic ecosystems bio-resources, introduction of new environmental agro- and biotechnologies, technologies of safety revival and soil fertility, energy-saving agricultural technologies, environmental and legal management in rural areas, monitoring and control of standards compliance, quality and safety of agricultural products, processed products and environment.

   The National University of Life and Environmental Sciences of Ukraine is one of the leading institutions of education, science and culture in Ukraine. More than 26 thousand students and more than 600 graduates, PhD students and seekers studyat 3 educational and research institutes and 13 departments of basic university institution (in Kyiv) and 10 separate units of NULES of Ukraine – regional universities of І-ІІІ accreditation levels.

   Educational process and scientific research at the University are provided by more than 2,600 scientific and educational and pedagogical workers, including about 300 professors and doctors of sciences, more than 1,000 assistant professors and PhDs.

   The Educational and Research Institute (ERI) of Energetics, Automatics and Energy Efficiency as a structural unit of the National University of Life and Environmental Sciences of Ukraine was created to train highly qualified specialists in energy and automation based on modern educational standards for public and private sectors of the economy.

   Over its 90-year history, our ERI has trained about 16,000 power and automation engineers.

   The scientific and innovative concept of the institute is to create the latest power tools, electrical technologies, automation systems, robotics, alternative power systems and their widespread introduction into production.

   ERI of Energetics, Automation and Energy Efficiency of NULES of Ukraine has strong ties with non-budget energy companies and companies-installers of electrical equipment, as well as training centers and complete about 12 organization. The survey among these companies will help to analyze of labour market and assessment of needs of VET for electrical engineering in the building sector. Among these institutions, we can also disseminate information about this project.

   In addition, an educational platform for advanced training of specialists in the field of electricity has created by our ERI. Every year about 250 people, including manufacturers and educators, undergo internships with us. They can be the target audience of the project.
',
                'website' => 'nubip.edu.ua/structure/nni-eae',
                'email' => 'wwwanten@gmail.com'
            ]
        ]
    ]
?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="my-3 my-lg-5">
         <div class=" mt-md-5  container  mb-5">
             <div class="row d-flex align-items-center justify-content-center">
                 <div class=" d-flex justify-content-center">
                     <h1 class="text-center text-primary"><?=t('Partners')?></h1>
                 </div>

                 <div class="my-4">
                     <img src="/assets/img/contact-bg.jpg" alt="" class="w-100" height="300px" style="object-fit: cover">
                 </div>

             </div>

             <h2 class="text-primary text-center my-4"><?=t('The project Consortium is composed of 9 organisations from 6 countries: Albania, Armenia, Bulgaria, Georgia, Turkey and Ukraine. Coordinator of the project is the Bulgarian partner Sofia energy center.')?></h2>

             <?php foreach ( $info as $mainTitle => $partner ) : ?>
             <h2><?=t($mainTitle)?></h2>
             <div class="divider-custom mt-3"></div>
             <?php foreach ( $partner as $ind => $row ) : ?>
                 <div class="bg-primary text-white p-4 my-4">
                     <div class="d-flex flex-row ">
                         <div class="">
                         <img style="max-height: 100px;object-fit: cover" src="<?=$row['img']?>" alt="">
                         </div>
                         <span style="font-size: 25px" class="ms-4 "><?=$row['title']?></span>
                     </div>
                         <?php if( array_key_exists('description',$row) ) : ?>
                     <div>
                         <pre class="text-white">
                             <?=$row['description']?>
                         </pre>
                     </div>
                         <?php endif; ?>

                    <div class="my-2">
                         <?php if( array_key_exists('website',$row) ) : ?>
                         <img src="/assets/img/website.png" alt="website" width="30px" class="me-2">
                         <a target="_blank" class="text-white" href="https://<?=$row["website"]?>"><?=$row["website"]?></a>
                         <?php endif; ?>
                     </div>
                     <div class="my-2">
                         <?php if( array_key_exists('email',$row) ) : ?>
                         <img src="/assets/img/email.png" alt="email" width="30px" class="me-2">
                             <a target="_blank" class="text-white" href="mail:<?=$row["email"]?>"><?=$row["email"]?></a>
                         <?php endif; ?>
                     </div>
                 </div>
             <?php endforeach; ?>
             <?php endforeach; ?>

         </div>
      </section>


      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
