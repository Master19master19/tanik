<?php include __DIR__ . '/config.php'; ?>
<?php
    $presentationGlob = glob( __DIR__ . '/assets/img/presentation/*.png' );
    foreach ( $presentationGlob as $key => $filePath ) {
        $res = explode( 'img/presentation' ,$filePath )[1];
        $presentationGlob[ $key ] = $res;
    }
?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="my-3 my-lg-5">
         <div class=" mt-md-5 mt-4 container">
             <div class="row d-flex align-items-center justify-content-center">
                 <div class="col-md-4 offset-lg-1 d-flex justify-content-center">
                     <p class="text-center">Project 101092256 – VET4GSEB – ERASMUS – EDU – 2022 – CB – VET: ” Vocational and Education Training partnership for Green, Smart Electricity in Buildings” , developed up to Call: ERASMUS-EDU-2022-CB-VET — Capacity Building in the field of Vocational Education and Training (VET).</p>
                 </div>

                 <div class="col-md offset-lg-1 d-flex justify-content-center ">
                     <img src="/assets/img/eu.jpeg" class="w-100" alt="eu-logo">
                 </div>
             </div>


             <div class="mt-4 mt-lg-5">
                 <div class="my-3 bg-primary text-white p-3">
                     <?=t('    It will be realized following the EU policies, initiatives and related documents in the field of: RES (Renewable energy sources) and technological innovations’ implementation in construction sector, digital and green skill upgrade of professionals via VET.')?>
                 </div>
                 <div class="my-2 bg-primary text-white p-3">
                     <?=t('    The main objective of the VET4GSEB project is to transfer the experience and good practices in the field of VET from the EU countries to 4 third countries participating in the project. To suipply the national VET providers with adapted solutions, training materials, case studies, tools and guidelines, which will allow them to update their training practices and programs in order to meet the skills needs of the labour market in the building and RES sector.')?>
                 </div>
                 <div class="my-2 bg-primary text-white p-3">
                     <?=t('    The objective will be reached by a series of activities using an innovative approach: trainers from VET providers from all participating countries will be provided with comprehensive information through an on-line networking Platform including: • Database of companies from the building and RES sector from all partners to update the information on the needs of the labour market in different countries; • Database of innovative training methods and tools, digital technologies implemented in the energy systems in smart buildings, innovative technical solutions for the application of PV systems in buildings; • Training materials on 5 modules and a Guidebook for the VET trainers.')?>
                 </div>
                 <div class="my-2 bg-primary text-white p-3">
                     <?=t('    The partners from Bulgaria and Turkey, owing extensive experience in VET regarding new training methods, soft skills, smart electrical installations and PV systems in buildings, incl. the necessary digital skills for them, will create training materials in order to transfer their knowledge to the participants from third countries not associated to the Programme: Albania, Armenia, Georgia and Ukraine, through the networking Platform.

')?>
                 </div>
                 <div class="my-2 bg-primary text-white p-3">
                     <?=t('    Trainings for VET providers from all participating countries will be performed in 2 phases: 1 pilot training in English for 18 trainers and 6 national trainings for 10 trainers each - in national languages.

')?>

                 </div>
                 <div class="my-2 bg-primary text-white p-3">
                     <?=t('    The expected results are 78 up-skilled VET providers who will improve their training practices and deliver updated training, adapted to the new needs of the market for electricians from the RES sector.

')?>

                 </div>
                 <div class="my-2 bg-primary text-white p-3">
                         The implementation of the planned projects; activities will be performed in 5 Work packages as follows:

                     <ul>
                         <li>WP1 - Project Management</li>
                        <li>WP2 - Needs analysis & current developments in VET</li>
                     <li>WP3 - Preparation of training tools & materials</li>
                     <li>WP4 - Train the trainers (Pilot training of 3 trainers per country + national training courses for 10 trainers per country)</li>
                     <li>WP3 - Preparation of training tools & materials</li>
                     <li>WP5 – Dissemination and Impact</li>
                     </ul>
                 </div>
                 <div class="my-2 bg-primary text-white p-3">
                     <?=t('The expected further impact from the training courses after the end of the project duration is 2400 trained installers per year.

')?>
                 </div>
             </div>

         </div>
          <div class="divider-custom my-5 container"></div>
          <div class="container ">
              <?php foreach ($presentationGlob as $imgSrc ) : ?>
                  <div class="my-3">
                      <img class="w-100" src="/assets/img/presentation/<?=$imgSrc?>" alt="" />
                  </div>
              <?php endforeach; ?>
          </div>
      </section>
      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
