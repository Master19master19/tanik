<?php include __DIR__ . '/config.php'; ?>
<?php
    $info = [
        '
The main objectives of this WP are to:
1. Coordinate VET4GSEB in the most effective manner;
2. Manage punctually the project budget;
3. Monitor and communicate project progress within VET4GSEB consortium and beyond (to the European Commission and external experts);
4. Organise in total 6 steering committee meetings with the entire consortium.
5. To decide on any questions/conflict resolutions that may arise over the course of the project. This work package will also ensure the quality of the work and deliverables, and will further allow effective risk management.',
        '
The objective of this WP is to collect data on the needs of the labour market for competent installers and electro technicians through involvement of the stakeholders (businesses and educational institutions) and on the basis of an in-depth analysis to identify the existing gaps in the current training programs. Data on the latest developments in VET and adult training in general, as well as on innovative solutions for PV systems in particular, energy management and smart electrical installations in buildings, will also be collected and analyzed in this Work Package for the purpose of elaborating an innovative ad hoc teaching methodology that will allow consortium’s working teams to develop the training program..',
        '
The aim of this WP is focusing on the development of training materials that will provide trainers of low voltage electrical professionals with labour market relevant skills (digital, soft, specific and pedagogical ones).
Its main objectives are:
1. development of a Networking Platform for network’s communications;
2. development of training Program and materials as lectures in PPP format in EN;
3. development of Guidebook for trainers;
4. adaptation and translation of all produced materials in the partners’ national languages..
',
'
WP4 includes organizing and conducting the ‘Train the trainer’ activities, designed and prepared to meet the needs identified in WP2 and in accordance with the latest developments in VET, and using the materials developed in WP3. The training is designed to up-skill trainers so as to align them with the latest developments in VET, continuous learning and adult learning, in particular, and enable them to train competent installers of building installations, competitive on the labour market..',
'
The specific objectives of WP5 are :
1. To achieve users friendly visual identity of the project objectives, activities, progress and outputs;
2. To promote the projects objectives, products, results etc. via all existing ( on-line, written and verbal) channels, information and communication tools and instruments;
3. To provide wide dissemination and efficient communication among the interested stakeholders in order to attract their interest and involve them actively in the project implementation and assure its continuation and sustainability.
'

    ]
?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="my-3 my-lg-5">
         <div class=" mt-md-5  container">
             <div class="row d-flex align-items-center justify-content-center">
                 <div class=" d-flex justify-content-center">
                     <h1 class="text-center text-primary"><?=t('Packages')?></h1>
                 </div>

             </div>


             <div class="mt-4 mt-lg-5 row justify-content-center">
                 <?php foreach ( $info as $index => $row ) : ?>
                 <div class=" col-xl-4 d-flex align-items-stretch">
                     <div class="my-3  bg-primary text-white p-3 p-lg-4 d-flex flex-column">
                         <h3 class="text-center ">WP<?=$index+1?></h3>
                            <pre class="mb-4 mb-lg-5">
                                <?=t($row)?>
                            </pre>
                         <button class="btn btn-light mt-auto"><?=t('Download')?></button>
                     </div>
                 </div>
                 <?php endforeach; ?>
             </div>

         </div>
      </section>
      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
