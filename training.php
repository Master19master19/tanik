<?php include __DIR__ . '/config.php'; ?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/partials/head.php'; ?>
   <body class="custom-scrollbar">
      <?php include __DIR__ . '/partials/header.php'; ?>
      <section class="d-flex justify-content-center align-items-center flex-column" style="background-image: url('/assets/img/training.jpg'); min-height: 500px;">
            <h1 class="text-white mb-4" style="font-size: 40px"><?=t('Training platform under construction')?></h1>
<!--          <button class="btn btn-primary">-->Connect<!--</button>-->
      </section>
      <?php include __DIR__ . '/partials/footer.php'; ?>
      <?php include __DIR__ . '/partials/scripts.php'; ?>
   </body>
</html>
